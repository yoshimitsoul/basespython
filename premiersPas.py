import random

quotes = [
    "Je ne viens pas sauver Rambo de la police, je viens sauver la police de Rambo !",
    "En ville, tu fais la loi. Ici, c'est moi. Alors fais pas chier. Fais pas chier ou je te ferai une guerre comme t'en as jamais vue.",
    "Pour survivre à la guerre, il faut devenir la guerre.",
    "Ce que vous appelez l’enfer, il appelle ça chez lui.",
    "J’ai toujours cru que c’était l’esprit la meilleure arme.",
    "Eux ont versé le premier sang pas moi.",
    "C’est pas nous qui le chassons, c’est lui qui nous chasse.",
    "Si quelqu’un… t’invite, à une soirée avec plein d’autres gens, et si toi t’y vas pas, personne ne remarque."
]

characters = [
    "Rambo",
    "Colonel Trautman"
]

def message(character, quote):
    n_character = character.capitalize()
    n_quote = quote.capitalize()
    return "{} a dit : {}".format(n_character, n_quote)

def get_random_item_in(my_list):
    rand_numb = random.randint(0, len(my_list) - 1) #choix de la ciation aléatoire
    item = my_list[rand_numb]
    return item  

user_answer = input('Tapez entrée pour connaître une autre citation ou B pour quitter le programme.')

while user_answer != "B":
    print(message(get_random_item_in(characters), get_random_item_in(quotes)))
    user_answer = input('Tapez entrée pour connaître une autre citation ou B pour quitter le programme.')
